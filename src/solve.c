#include <gdnative_api_struct.gen.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "search.h"

const godot_gdnative_core_api_struct *api = NULL;
const godot_gdnative_ext_nativescript_api_struct *nativescript_api = NULL;

GDCALLINGCONV void *cubesolve_constructor(godot_object *obj, void *method_data) {
	//printf("test.constructor()\n");
	return 0;
}

GDCALLINGCONV void cubesolve_destructor(godot_object *obj, void *method_data, void *user_data) {
	api->godot_free(user_data);
	//printf("test.destructor()\n");
}

/** func _ready() **/
godot_variant cubesolve_ready(godot_object *obj, void *method_data, void *user_data, int num_args, godot_variant **args) {
        godot_variant ret;
        api->godot_variant_new_nil(&ret);

        //printf("_ready()\n");

        return ret;
}

/** Library entry point **/
void GDN_EXPORT godot_gdnative_init(godot_gdnative_init_options *o) {
	api = o->api_struct;

	// Find NativeScript extensions.
	for (int i = 0; i < api->num_extensions; i++) {
		switch (api->extensions[i]->type) {
			case GDNATIVE_EXT_NATIVESCRIPT: {
				nativescript_api = (godot_gdnative_ext_nativescript_api_struct *)api->extensions[i];
			}; break;
			default:
				break;
		};
	};
}

/** Library de-initialization **/
void GDN_EXPORT godot_gdnative_terminate(godot_gdnative_terminate_options *o) {
	api = NULL;
	nativescript_api = NULL;
}

/** Script entry (Registering all the classes and stuff) **/
void GDN_EXPORT godot_nativescript_init(void *desc) {
	//printf("nativescript init\n");

	godot_instance_create_func create_func = {
		.create_func = &cubesolve_constructor,
                .method_data = 0,
                .free_func   = 0
        };

        godot_instance_destroy_func destroy_func = {
                .destroy_func = &cubesolve_destructor,
                .method_data  = 0,
                .free_func    = 0
        };

        nativescript_api->godot_nativescript_register_class(desc, "CUBESOLVE", "Node", create_func, destroy_func);

        {
                godot_instance_method method = {
                        .method = &cubesolve_ready,
                        .method_data = 0,
                        .free_func = 0
                };

                godot_method_attributes attr = {
                        .rpc_type = GODOT_METHOD_RPC_MODE_DISABLED
                };

                nativescript_api->godot_nativescript_register_method(desc, "CUBESOLVE", "_ready", attr, method);
        }
}

char *gd_variant_cpy(char *dest, godot_variant *src){
	godot_string str1 = api->godot_variant_as_string(src);
	godot_char_string str2 = api->godot_string_ascii(&str1);
	strcpy(dest,api->godot_char_string_get_data(&str2));
	return dest;
}

godot_variant GDN_EXPORT cubesolve(void *data, godot_array *args) {
	godot_variant ret;
	api->godot_variant_new_nil(&ret);
	godot_variant array_size;
	api->godot_variant_new_int(&array_size, api->godot_array_size(args));
	uint64_t size_array = api->godot_variant_as_uint(&array_size);
	api->godot_variant_destroy(&array_size);
	if(size_array > 0){
		char arg1[64];
		char arg2[64];
		char patternized[64];
		godot_variant variant = api->godot_array_get(args,0);
		gd_variant_cpy(arg1,&variant);
		char* facelets = arg1;
		//printf("%s|%u\n",facelets,(unsigned)strlen(arg1));
		if (size_array > 1) {
			godot_variant variant2 = api->godot_array_get(args,1);
			gd_variant_cpy(arg2,&variant2);
			patternize(facelets, arg2, patternized);
			facelets = patternized;
		}
		char *sol = solution(
			facelets,
			24,
			1000,
			0,
			"cache"
		);
		if (sol == NULL) {
			//api->godot_string_parse_utf8(&str,"Unsolvable cube!");
			//api->godot_print(&str);
			//api->godot_string_destroy(&str);
			return ret;
		}
		godot_string str;
        api->godot_string_new(&str);
        api->godot_string_parse_utf8(&str, sol);
        //api->godot_print(&str);
        api->godot_variant_new_string(&ret, &str);
        api->godot_string_destroy(&str);
        free(sol);
	}
	return ret;	
}