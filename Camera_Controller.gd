extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var click = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
#	var pivot_point = get_node("../Rubik\'s_cube").translation
#	$Camera.look_at(pivot_point,Vector3.UP)
	pass

func _input(event):
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT:
			if event.pressed:
				click = true
#				print("Left button was clicked at ", event.position)
			else:
				click = false
#				print("Left button was released")
#		print("Mouse Click/Unclick at: ", event.position)
		
	if event.is_action_pressed("ui_up"):
		rotation += Vector3(PI/2,0,0)
	elif event.is_action_pressed("ui_down"):
		rotation -= Vector3(PI/2,0,0)
	elif event.is_action_pressed("ui_left"):
		rotation -= Vector3(0,PI/2,0)
	elif event.is_action_pressed("ui_right"):
		rotation += Vector3(0,PI/2,0)
	elif event.is_action_pressed("ui_accept"):
		rotation = Vector3.ZERO

func _unhandled_input(event):
	if event is InputEventMouseMotion:
		if click :
			rotation += Vector3(sign(-event.relative.y)/25,sign(-event.relative.x)/25,0)
