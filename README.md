# Rubik's cube solver

This is a rubik's solver with a GUI made in godot with gdscript and GDNative C for the code solve portion using the code from [muodov](https://github.com/muodov/kociemba).

This project can be ran on godot 3.3.4 with no additional modules.

## Compile with SCons (cross platform)
if a modification to the C code is required, the usage of sCons is preferred as it is the simplest method and the file has already been configured.

Dependencies:
 * `clang`, `gcc`, or any decent C compiler that's C11 compatible.

You can use SCons to compile the library if you have it installed:

```
scons platform=PLATFORM
```
## Disclaimer 
**Use this code at your own risk**
