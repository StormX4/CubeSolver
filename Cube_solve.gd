extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var cube_data = $"Rubik's_cube"
onready var C_code = preload("res://cubesolve.tres")
var sol = null
var move = null

# Called when the node enters the scene tree for the first time.
# The thread will start here.
func _ready():
#	test code 
#	cube_data._randomize(25)
#	var start = OS.get_ticks_usec()
#	apply_cube_algorithm(get_solution(cube_data._state_to_string()))
#	var end = OS.get_ticks_usec()
#	var time = (end-start)/1000000.0
#	print("solve_time:",time," seconds")
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if sol != null and !sol.empty():
		if move == null :
			$"Label2".text = "current move: " + sol[0]
			move = convert_to_move(sol[0])
		
		if move != null and cube_data._rotation(move[0],move[1],$"VSlider".value) :
			sol.remove(0)
			move = null
		
		if sol.empty() :
			$"Label2".text =""
			sol = null

func find_mod(a:float,b:float) -> float:
	while a >= abs(b) or a <= -abs(b) :
		a -= (sign(a)*abs(b))
	return a

func get_solution(start_state,end_state=null):
	var states = []
	var res = null
	if start_state is String:
		states.append(start_state)
		if end_state is String:
			states.append(end_state)
		var gdn = GDNative.new()
		gdn.library = C_code
		gdn.initialize()
		res = gdn.call_native("standard_varcall", "cubesolve", states)
		gdn.terminate()
	return res

func convert_to_move(move:String):
	var conversion_table = ["U","R","F","D","L","B"]
	var face = null
	var desired_angle = null
	var rot_axis
	var dir
	if move.length() > 0 :
		face = conversion_table.find(move.substr(0,1))
		rot_axis = cube_data.centers[face].translation.abs().max_axis()
		dir = -cube_data.centers[face].translation.normalized()[rot_axis]
		desired_angle = ( cube_data.centers[face].rotation[rot_axis] + (PI/2.00*dir) )
		if move.length() > 1 :
			match(move.substr(1,1)):
				"'": 
					desired_angle -= (PI*dir)
				"2":
					desired_angle += (PI/2.00*dir)
		desired_angle = find_mod(desired_angle,2.00*PI)
	return [face,desired_angle] if (face != null and desired_angle!= null) else null

func apply_cube_algorithm(moves:String):
	var moveList = moves.split(" ") 
	for idx in moveList.size():
		var move = convert_to_move(moveList[idx])
		if move != null : 
#			print("face: ",cube_data.eColour.keys()[move[0]], " | angle: ",move[1],"| initial_angle: ",cube_data.centers[face].rotation[rot_axis])
			cube_data._rotation(move[0],move[1])

func reset(start_state):
	for cubie in cube_data.pieces.size() :
		cube_data.pieces[cubie].translation = start_state[cubie][0]
		cube_data.pieces[cubie].rotation = start_state[cubie][1]

func _on_BtnCheckStart_pressed():
	if sol == null :
		sol = get_solution(cube_data._state_to_string())
		if sol != null :
			sol = sol.split(" ")
			sol.remove(sol.size()-1)

func _on_VSlider_value_changed(value):
	$"RichTextLabel".text = String(value)
