extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
const MARGIN = 0.001
const MOVE_ROTATION = PI/2.00
enum eColour{
	YELLOW=0,
	ORANGE=1,
	GREEN=2,
	WHITE=3,
	RED=4,
	BLUE=5
}

var pieces = [
#	egdes 2 faces
	$Cube003,
	$Cube005,
	$Cube021,
	$Cube,
	$Cube006,
	$Cube011,
	$Cube019,
	$Cube010,
	$Cube002,
	$Cube023,
	$Cube015,
	$Cube009,
#	Corners 3 faces
#	-90 dg equals y axis shifting the list to the left by -1
	$Cube017,
	$Cube022,
	$Cube020,
	$Cube012,
	$Cube001,
	$Cube016,
	$Cube024,
	$Cube008
]
# Y O G W R B
var centers = [
#	xyz of all the centers
	$Cube014,  # YELLOW
	$Cube018,  # ORANGE
	$Cube013,  # GREEN
	$Cube025, # WHITE
	$Cube007,  # RED
	$Cube004 # BLUE
]
# TO-DO: implement code in a way that makes this varible unique to the specific instance of the function
# BUG: The spin in the final call of the function doesn't stop.
#var final_pos = Vector3.ZERO
var angle = PI
var pieceIndex = []
var currFace = -1

# Called when the node enters the scene tree for the first time.
func _ready():
	pieceIndex.clear()
	for center in centers.size() :
		centers[center].translation = centers[center].translation.round()
		pieceIndex = _findPieces(center)
		for idx in pieceIndex :
			var child = Spatial.new()
			child.name = String(eColour.keys()[center])
			pieces[idx].add_child(child)
			child.translation = centers[center].translation.normalized()
	_rotation(0,0)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if currFace != -1:
		if(_rotation(currFace,angle,$"../VSlider".value)):
			currFace = -1

#       number of scramble moves
func _randomize(moves):
	for i in moves :
		_rotation(randi()%6,(randi()%4)*(MOVE_ROTATION),1)

#       face:eCoulour desired_angle:radian angle delta: between 0 and 1
func _rotation(face,desired_angle,delta=1)->bool:
	if face >= eColour.YELLOW && face <= eColour.BLUE :
		var final_pos = Vector3(desired_angle,desired_angle,desired_angle) 
#		At first execution of this function (until it's completion)
		if pieceIndex.empty() :
			pieceIndex = _findPieces(face)
			for p in pieceIndex:
				_changeParent(centers[face],pieces[p])
#		To make sure that all the pieces of the face are aligned 
		if pieceIndex.size() == 8 :
#		Find the axis of rotaton based on the face to rotate
			var rot_axis = (face%3+1)%2 if face%3 < 2 else face%3
#			var float_pos = (sign(final_pos[rot_axis]) if sign(final_pos[rot_axis]) !=0 else 1 *2*PI) - abs(final_pos[rot_axis]) if abs(final_pos[rot_axis] - centers[face].rotation[rot_axis]) >= (3*PI/2) else 
#		Rotate the pieces of the face according to it's axis of rotation 
			centers[face].rotation[rot_axis] = lerp(centers[face].rotation[rot_axis],final_pos[rot_axis],delta)
			if _approx_equal(centers[face].rotation[rot_axis], final_pos[rot_axis]):
				centers[face].rotation[rot_axis] = round(final_pos[rot_axis]/(MOVE_ROTATION))*(MOVE_ROTATION)
				_finaliseFacePiecesState()
				return true
		else:
			_finaliseFacePiecesState()
	return false

func _approx_equal(val1,val2) -> bool:
#	If the 2 values are within margin of eachother 
#	return true
	return abs(val1 - val2) < MARGIN
	
func _finaliseFacePiecesState():
#	Change the parent of all the pieces in the found pieces array back to it's initial value
	for p in pieceIndex:
		_changeParent(self,pieces[p])
		pieces[p].translation = pieces[p].translation.round()
		pieces[p].rotation_degrees = pieces[p].rotation_degrees.round()
	pieceIndex.clear()

func _changeParent(new_parent,node):
#	Get the position of the node to be reassigned
	var global_pos = node.global_transform
#	Unassign it from the current parent
	node.get_parent().remove_child(node)
#	Assign it to the new parent
	new_parent.add_child(node)
#	Set the position of the node that's been reassigned
	node.global_transform = global_pos
#	print(node.get_parent())

func _findPieces(face) -> Array:
#	If the face exists
	var facePieces = []
	if face >= eColour.YELLOW && face <= eColour.BLUE :
#		Get every piece on the desired face
		for p in pieces.size():
			if ((is_equal_approx(centers[face].translation.x,pieces[p].translation.x) && 
				!is_zero_approx(centers[face].translation.x)) || 
				(is_equal_approx(centers[face].translation.y,pieces[p].translation.y) && 
				!is_zero_approx(centers[face].translation.y)) || 
				(is_equal_approx(centers[face].translation.z,pieces[p].translation.z) &&
				!is_zero_approx(centers[face].translation.z))): 
				facePieces.append(p)
	return facePieces

# *             |*U1**U2**U3*|
# *             |************|
# *             |*U4**U5**U6*|
# *             |************|
# *             |*U7**U8**U9*|
# *             |************|
# * ************|************|************|************|
# * *L1**L2**L3*|*F1**F2**F3*|*R1**R2**F3*|*B1**B2**B3*|
# * ************|************|************|************|
# * *L4**L5**L6*|*F4**F5**F6*|*R4**R5**R6*|*B4**B5**B6*|
# * ************|************|************|************|
# * *L7**L8**L9*|*F7**F8**F9*|*R7**R8**R9*|*B7**B8**B9*|
# * ************|************|************|************|
# *             |************|
# *             |*D1**D2**D3*|
# *             |************|
# *             |*D4**D5**D6*|
# *             |************|
# *             |*D7**D8**D9*|
# *             |************|
# Y O G W R B
# *A cube definition string "UBL..." means for example: In position U1 we have the U-color, in position U2 we have the
# * B-color, in position U3 we have the L color etc. according to the order U1, U2, U3, U4, U5, U6, U7, U8, U9, R1, R2,
# * R3, R4, R5, R6, R7, R8, R9, F1, F2, F3, F4, F5, F6, F7, F8, F9, D1, D2, D3, D4, D5, D6, D7, D8, D9, L1, L2, L3, L4,
# * L5, L6, L7, L8, L9, B1, B2, B3, B4, B5, B6, B7, B8, B9 of the enum constants.

var lookup_table = {
	"Y":"U",
	"O":"R",
	"G":"F",
	"W":"D",
	"R":"L",
	"B":"B"
}

func _state_to_string(forSolve:bool=true) -> String:
	var state:String
# Y good O bad G good
# W bad R good B bad
	for face in eColour:
		var facePieces = _findPieces(eColour[face])
		var axis = centers[eColour[face]].translation.abs().max_axis()
		var pos = centers[eColour[face]].translation.normalized()
#				AXIS_X = 0
#				AXIS_Y = 1
#				AXIS_Z = 2
		var plane2d = { y = (axis+2)%3,x = (axis+1)%3} if axis != Vector3.AXIS_Z else { x = (axis+2)%3,y = (axis+1)%3}
		var invert = -1 if eColour[face]%2 == 1 else 1
#				axis  %3
#				Z axis   X axis   Y axis
#				X axis+1 Z axis+2 Z axis+1
#				Y axis+2 Y axis+1 X axis+2
		var OrderedList = [facePieces[0]]
#		Order the values for the string
#		print(face)
		for idx in range(1,facePieces.size()) :
			var idxPrevious = OrderedList.size()-1
			while idxPrevious > -1 :
				if compare_cubie(
					Vector2(pieces[OrderedList[idxPrevious]].translation[plane2d.x] * invert,
							pieces[OrderedList[idxPrevious]].translation[plane2d.y]),
					Vector2(pieces[facePieces[idx]].translation[plane2d.x] * invert,
							pieces[facePieces[idx]].translation[plane2d.y])) :
					OrderedList.insert(idxPrevious+1,facePieces[idx])
					break
				idxPrevious-=1
			if idxPrevious == -1 :
				OrderedList.insert(0,facePieces[idx])
		if pos.y >= 0 && invert == -1 :
			OrderedList.invert()
#		Check colour of piece 
		for idx in OrderedList.size() :
			if idx == (OrderedList.size()/2):
				state += lookup_table[face[0]] if forSolve else face[0]
			# check colour of face on side of cube
			for child in pieces[OrderedList[idx]].get_children() :
				var posChild = child.to_global(Vector3.ZERO) 
				posChild = Vector3( int(posChild.x/3),
									int(posChild.y/3),
									int(posChild.z/3) )
				if pos.abs() == posChild.abs() :
					state += lookup_table[child.name[0]] if forSolve else child.name[0]
					break
	return state

# checks if cubie 1 is earlier(closer to the top left of the face) than cubie 2
func compare_cubie(cubie1:Vector2,cubie2:Vector2):
	if(cubie1.direction_to(cubie2) <= Vector2.ZERO):
		return true
	return false

func get_final_pos(piece) -> Vector3:
	var pos = Vector3.ZERO
	var cubeCenters = eColour.keys()
	for colour in piece.get_children() :
		for face in centers.size() :
#			Add the position of every face that composes te piece
			if (face >= eColour.YELLOW && face <= eColour.BLUE &&
				colour.name == cubeCenters[face]) :
				pos += centers[face][0].translation
	return pos

func is_completed():
	for cubie in pieces : 
		if ( !( cubie.translation.is_equal_approx(get_final_pos(cubie)) && 
				cubie.rotation.is_equal_approx(Vector3.ZERO) ) ):
				return false
	return true

func _on_RotationSlider_value_changed(value):
	angle = value*(MOVE_ROTATION)
	print(angle)

func _on_FaceList_item_activated(index):
	if currFace == -1 :
		currFace = index
	else:
		print("Current action isn't completed")

func _on_BtnRandom_pressed():
	randomize()
	_randomize(25)
